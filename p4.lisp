(defun to-digits (n)
  (do ((n n (floor (/ n 10)))
       (digits '() (cons (rem n 10) digits)))
      ((< n 1) digits)))

(defun range (lower upper)
  (loop for x from lower upto upper collect x))

(defun count-digits (digits)
  (let ((table (make-hash-table)))
    (loop for digit in digits
       do
         (incf (gethash digit table 0))
       finally
         (return table))))

(defun has-exactly-two-digits (digits)
  (let ((table (count-digits digits)))
    (loop for k being the hash-keys of table
       do
         (when (= 2 (gethash k table))
           (return t)))))

(defun print-hash-table (table)
  (loop for key being the hash-key of table
     do
       (format t "~a -> ~a~%" key (gethash key table))))

(defun has-double-digits (digits)
  (some #'= digits (cdr digits)))

(defun has-no-more-than-two-digits (digits)
  (mapcar #'= digits (cdr digits) (cdr (cdr digits))))

(defun has-always-increasing-digits (digits)
  (every (lambda (x y) (or (< x y) (= x y))) digits (cdr digits)))

(defparameter *puzzle-lower* 382345)
(defparameter *puzzle-upper* 843167)

(defun solution (lower upper)
  (length
   (remove-if-not #'has-exactly-two-digits
                  (remove-if-not #'has-always-increasing-digits
                                 (mapcar #'to-digits (range lower upper))))))

                        
                        
      
