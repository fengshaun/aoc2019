(defun calculate-fuel-cost (mass &optional (fuel 0))
  (+ fuel (max (- (floor (/ mass 3)) 2) 0)))

(defun get-input-as-list (input-filepath)
  (with-open-file (filestream input-filepath :direction :input)
    (loop for line = (read-line filestream nil 'eof)
       until (eq line 'eof)
       collect (parse-integer line))))

(defun run-part-one (input-filepath)
  (loop for mass in (get-input-as-list input-filepath)
       sum (calculate-fuel-cost mass)))

(defun calculate-all-fuel-cost (initial-mass)
  (do* ((fuel (calculate-fuel-cost initial-mass) (calculate-fuel-cost fuel))
        (total-fuel fuel (+ total-fuel fuel)))
       ((= fuel 0) total-fuel)))

(defun run-part-two (input-filepath)
  (loop for mass in (get-input-as-list input-filepath)
       sum (calculate-all-fuel-cost mass)))

(defun test-predefined-results-part-one ()
  (assert (= (calculate-fuel-cost 12) 2))
  (assert (= (calculate-fuel-cost 14) 2))
  (assert (= (calculate-fuel-cost 1969) 654))
  (assert (= (calculate-fuel-cost 100756) 33583)))

(defun test-predefined-results-part-two ()
  (assert (= (calculate-all-fuel-cost 14) 2))
  (assert (= (calculate-all-fuel-cost 1969) 966))
  (assert (= (calculate-all-fuel-cost 100756) 50346)))
