(defun get-input-as-list (filepath)
  (with-open-file (stream filepath :direction :input)
    (mapcar #'parse-integer (uiop:split-string (read-line stream) :separator ","))))

(defun preprocess-intcode (intcode noun verb)
  (let ((new-intcode (copy-seq intcode)))
    (progn
      (setf (nth 1 new-intcode) noun)
      (setf (nth 2 new-intcode) verb)
      new-intcode)))

(defun run-intcode (intcode)
  (do* ((current-position 0 (+ 4 current-position)))
       ((= (nth current-position intcode) 99) intcode)
    (let* ((opcode (nth current-position intcode))
           (input-position-1 (nth (+ 1 current-position) intcode))
           (input-1 (nth input-position-1 intcode))
           (input-position-2 (nth (+ 2 current-position) intcode))
           (input-2 (nth input-position-2 intcode))
           (output-position  (nth (+ 3 current-position) intcode)))
      (when (= 99 opcode) (return intcode))
      (setf (nth output-position intcode)
            (funcall (cond
                       ((= 1 opcode) #'+)
                       ((= 2 opcode) #'*)) input-1 input-2)))))

(defun test-run-intcode ()
  (assert (equalp (run-intcode '(1 0 0 0 99)) '(2 0 0 0 99)))
  (assert (equalp (run-intcode '(2 3 0 3 99)) '(2 3 0 6 99)))
  (assert (equalp (run-intcode '(2 4 4 5 99 0)) '(2 4 4 5 99 9801)))
  (assert (equalp (run-intcode '(1 1 1 4 99 5 6 0 99)) '(30 1 1 4 2 5 6 0 99))))

(defun part-one-solution (input-filepath)
  (car (run-intcode (preprocess-intcode (get-input-as-list input-filepath) 12 2))))

(defun find-noun-and-verb (initial-intcode)
  (loop named outer for noun from 0 to 99 do
       (loop for verb from noun to 99 do
            (when (= (car (run-intcode (preprocess-intcode initial-intcode noun verb)))
                     19690720)
              (return-from outer (+ (* 100 noun) verb))))))

(defun part-two-solution (input-filepath)
  (find-noun-and-verb (get-input-as-list input-filepath)))
