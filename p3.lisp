(ql:quickload 'alexandria)
(ql:quickload 'array-operations)

(declaim (optimize (speed 0) (debug 3)))

(defun get-input (filepath)
  (with-open-file (stream filepath :direction :input)
    (loop for line = (read-line stream nil 'eof)
       until (eql line 'eof)
         collect line)))

(defun sum (xs)
  (loop for x in xs sum x))

(defstruct point
  (x 0)
  (y 0))

(defstruct line
  (direction 'up)
  (length 0)
  (origin (make-point :x 0 :y 0))
  (end    (make-point :x 0 :y 0)))

(defun line-is-vertical (line)
  (or (eql 'up (line-direction line))
      (eql 'down (line-direction line))))

(defun line-is-horizontal (line)
  (not (line-is-vertical line)))

(defun line-start-x (line)
  (apply #'min (mapcar #'point-x (list (line-origin line) (line-end line)))))

(defun line-end-x (line)
  (apply #'max (mapcar #'point-x (list (line-origin line) (line-end line)))))

(defun line-start-y (line)
  (apply #'min (mapcar #'point-y (list (line-origin line) (line-end line)))))

(defun line-end-y (line)
  (apply #'max (mapcar #'point-y (list (line-origin line) (line-end line)))))

(defun parse-line (p)
  (make-line
   :direction (let ((dir-letter (subseq p 0 1)))
                (cond
                  ((string= dir-letter "U") 'up)
                  ((string= dir-letter "D") 'down)
                  ((string= dir-letter "L") 'left)
                  ((string= dir-letter "R") 'right)))
   :length (parse-integer (subseq p 1))))

(defun parse-path (path)
    (mapcar #'parse-line (uiop:split-string path :separator ",")))
       
(defun path-to-lines (path)
  (let ((point (make-point :x 0 :y 0)))
    (loop for line in path
       collect 
         (make-line
                :direction (line-direction line)
                :length (line-length line)
                :origin (copy-point point)
                :end (case (line-direction line)
                       (left   (setf point (make-point :x (- (point-x point) (line-length line)) :y (point-y point))))
                       (right  (setf point (make-point :x (+ (point-x point) (line-length line)) :y (point-y point))))
                       (up     (setf point (make-point :x (point-x point) :y (+ (point-y point) (line-length line)))))
                       (down   (setf point (make-point :x (point-x point) :y (- (point-y point) (line-length line))))))))))

(defun do-lines-intersect (&key vertical horizontal)
  (and (<= (line-start-x horizontal)
           (point-x (line-origin vertical))
           (line-end-x   horizontal))
       (<= (line-start-y vertical)
           (point-y (line-origin horizontal))
           (line-end-y   vertical))))

(defun find-intersections (lines1 lines2)
  (loop for line1 in lines1
     collect
       (loop for line2 in lines2
          collect
            (cond
              ((and (line-is-vertical line1)
                    (line-is-horizontal line2)
                    (do-lines-intersect :vertical line1 :horizontal line2))
               (make-point :x (line-start-x line1) :y (line-start-y line2)))
              ((and (line-is-vertical line2)
                    (line-is-horizontal line1)
                    (do-lines-intersect :vertical line2 :horizontal line1))
               (make-point :x (line-start-x line2) :y (line-start-y line1)))
              (t (make-point :x 0 :y 0))))
     into all-results
     finally
       (return (remove-if (lambda (p) (equalp p (make-point :x 0 :y 0)))
                          (loop for point in all-results appending point)))))

(defun find-closest-manhattan-intersection (all-results)
  (apply #'min (mapcar (lambda (p) (+ (abs (point-x p)) (abs (point-y p)))) all-results)))

(defun part-one-solution (input-file-path)
  (find-closest-manhattan-intersection (apply #'find-intersections
                                              (mapcar #'path-to-lines
                                                      (mapcar #'parse-path
                                                              (get-input input-file-path))))))

(defun has-reached-intersection (line intersection)
  (cond
    ((line-is-vertical line) (and (= (line-start-x line) (point-x intersection))
                                  (<= (line-start-y line)
                                      (point-y intersection)
                                      (line-end-y line))))
    ((line-is-horizontal line) (and (= (line-start-y line) (point-y intersection))
                                    (<= (line-start-x line)
                                        (point-x intersection)
                                        (line-end-x line))))
    (t nil)))

(defun count-steps (line intersection)
  (cond
    ((not (has-reached-intersection line intersection)) (line-length line))
    ((line-is-vertical   line) (abs (- (point-y intersection) (point-y (line-origin line)))))
    ((line-is-horizontal line) (abs (- (point-x intersection) (point-x (line-origin line)))))
    (t (break))))

(defun steps-to-intersection (lines intersection)
  (loop for line in lines
     until (has-reached-intersection line intersection)
     sum (count-steps line intersection)
     into total-steps
     finally (return (+ total-steps (count-steps line intersection)))))

(defun get-all-steps-to-intersections (lines1 lines2)
  (let ((intersections (find-intersections lines1 lines2)))
    (loop for intersection in intersections
       collect (+ (steps-to-intersection lines1 intersection)
                  (steps-to-intersection lines2 intersection)))))

(defun minimize-path-to-intersection (steps-to-intersections)
  (apply #'min steps-to-intersections))

(defun part-two-solution (input-file-path)
  (minimize-path-to-intersection (apply #'get-all-steps-to-intersections
                                        (mapcar #'path-to-lines
                                                (mapcar #'parse-path (get-input input-file-path))))))
